FROM openjdk:8-jre

WORKDIR /app

COPY prod/ .
COPY conf/application.conf .
EXPOSE 9001
ENV HTTP_PORT=9001
#CMD ./bin/webcontroller -Dconfig.file=prod.conf
RUN ls -la
RUN pwd
CMD ./start
