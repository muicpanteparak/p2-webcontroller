package modules

import com.google.inject.AbstractModule

class Module extends AbstractModule{
  override def configure(): Unit = {
    bind(classOf[Singleton]) asEagerSingleton()
  }
}
