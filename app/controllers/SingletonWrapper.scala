package controllers

import javax.inject.{Inject, Singleton}
import me.teparak.minio.utils.MinioWrapper
import org.mongodb.scala.bson.collection.immutable.Document
import org.mongodb.scala.{MongoClient, MongoCollection, MongoDatabase}
import play.api.Logger

@Singleton
class SingletonWrapper @Inject() (configuration: play.api.Configuration){

  private val DB_HOST = configuration.underlying.getString("mongo.url")

  private val MO_HOST = configuration.underlying.getString("minio.url")
  private val MO_PORT = configuration.underlying.getInt("minio.port")

  private val DB_NAME = "pdf2text"

  private val COL_NAME = "jobs"

  val MS_HOST = configuration.underlying.getString("master.url")

  val INIT_QUEUE_NAME = "init_queue"
  val FILE_NAME = "tarball"

  val minio: MinioWrapper = new MinioWrapper(s"$MO_HOST",MO_PORT)
  Logger.debug(s" [x] MINIO  Connected at $MO_HOST")

  val mongo: MongoClient = MongoClient(s"mongodb://$DB_HOST")
  Logger.debug(s" [x] MONGO  Connected at $DB_HOST")

  private val database: MongoDatabase = mongo.getDatabase(DB_NAME)
  val collection: MongoCollection[Document] = this.database.getCollection(COL_NAME)
}
