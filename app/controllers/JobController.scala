package controllers

import java.io.{File, FileInputStream}
import java.nio.file.{Files, Path}
import java.util.UUID

import akka.stream.IOResult
import akka.stream.scaladsl._
import akka.util.ByteString
import com.google.gson.JsonObject
import com.mongodb.client.model.Filters._
import javax.inject.Inject
import org.mongodb.scala._
import org.mongodb.scala.model.Projections._
import play.api.libs.json.{JsValue, Json}
import play.api.libs.streams.Accumulator
import play.api.libs.ws.WSClient
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc._
import play.api.{Logger, mvc}
import play.core.parsers.Multipart.FileInfo

import scala.concurrent.{ExecutionContext, Future}

class JobController @Inject()
(cc: MessagesControllerComponents, singleton: SingletonWrapper, ws: WSClient)
(implicit executionContext: ExecutionContext) extends MessagesAbstractController(cc) {

  def getFile(job_id: String): mvc.Action[AnyContent] =
    Action.async(parse.default) { implicit request => {
      singleton.mongo.startSession()
      singleton.collection.find(in("job_id", job_id))
        .projection(include("object_id", "file_name"))
        .first()
        .toFuture()
        .map(d => {
          val oid = d.getString("object_id")
          val fn = d.getString("file_name")
          try {
            val stream = singleton.minio.download(job_id, s"$fn-final")
            val dataContent: Source[ByteString, _] = StreamConverters.fromInputStream(() => stream)
            Ok.chunked(dataContent)
              .withHeaders(("Content-Disposition", s"filename=$fn"))
          } catch {
            case _: Throwable => BadRequest
          }
        })
    }
    }


//  def health = Action{ _ => Ok("healthy") }

  def getJob: Action[JsValue] =
    Action.async(parse.json) { implicit request => {
      singleton.mongo.startSession()
      val job_id = (request.body \ "job_id").as[String]
      Logger.debug(s"[x] Requested ID Check $job_id")
      singleton.collection.find(in("job_id", job_id))
        .toFuture()
        .map(_.nonEmpty)
        .map(res => Json.toJson(Map("found" -> res)))
        .map({
          //          singleton.mongo.close()
          Ok(_)
        })
    }
    }

  def index1: mvc.Action[AnyContent] = Action { implicit request => {
    val r: Result = Ok(Json.toJson(Map("name" -> "Nut")))
    r
  }
  }

  type FilePartHandler[A] = FileInfo => Accumulator[ByteString, FilePart[A]]

  private def handleFilePartAsFile: FilePartHandler[File] = {
    case FileInfo(partName, filename, contentType) =>
      val path: Path = Files.createTempFile("multipartBody", "tempFile")
      val fileSink: Sink[ByteString, Future[IOResult]] = FileIO.toPath(path)
      val accumulator: Accumulator[ByteString, IOResult] = Accumulator(fileSink)
      accumulator.map {
        case IOResult(_, _) => FilePart(partName, filename, contentType, path.toFile)
      }
  }

  /*
  * Delete temp file after completion
  * */
  private def operateOnFile(file: File): Long = {
    val size = Files.size(file.toPath)
    Files.deleteIfExists(file.toPath)
    size
  }

  def submit = Action(parse.multipartFormData) { request =>

    val jobId: String = singleton.minio.getBucket

    Logger.debug(s"=== Received Job $jobId ===")

    val objectId = UUID.randomUUID().toString

    val collection: MongoCollection[Document] = singleton.collection

    val json: JsonObject = new JsonObject()
    request.body.file(singleton.FILE_NAME).foreach { tarball =>
      val fis: FileInputStream = new FileInputStream(tarball.ref)
      singleton.minio.upload(jobId, objectId, fis, tarball.ref.length())

      val fn: String = tarball.filename
      json.addProperty("file_name", fn)
      json.addProperty("job_id", jobId)
      json.addProperty("object_id", objectId)
      val doc = Document(
        "file_name" -> fn,
        "job_id" -> jobId,
        "object_id" -> objectId
      )

      val insertObservable: SingleObservable[Completed] = collection
        .insertOne(doc)

      insertObservable.subscribe(new Observer[Completed] {
        override def onNext(result: Completed): Unit = println(s"onNext: $result")

        override def onError(e: Throwable): Unit = println(s"onError: $e")

        //        override def onComplete(): Unit = singleton.mongo.close()
        override def onComplete(): Unit = println("complete")
      })

      operateOnFile(tarball.ref)
    }
    val data = Json.obj(
      "job_id" -> jobId,
      "object_id" -> objectId
    )
    ws.url(s"${singleton.MS_HOST}/job/create").post(data)

    Logger.debug(s"=== Job $jobId Queued ===")
    Ok(Json.parse(json.toString))
  }
}
