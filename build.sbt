name := """WebController"""
organization := "com.muic.backendtech"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

resolvers += "Artifactory" at "https://artifact.teparak.me/artifactory/backendtech/"
fork := true // required for "sbt run" to pick up javaOptions
javaOptions += "-Dplay.editor=http://localhost:63342/api/file/?file=%s&line=%s"
libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += ws

libraryDependencies += "me.teparak" % "minio" % "1.2.1"

libraryDependencies ++= Seq("org.mongodb.scala" %% "mongo-scala-driver" % "2.4.2", "org.mongodb" % "mongo-java-driver" % "3.8.2")


// Assembly setting
//assemblyJarName in assembly := "webcontroller-1.0.jar"
//assemblyMergeStrategy in assembly := {
//  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
//  case x => MergeStrategy.first
//}
libraryDependencies += "com.google.code.gson" % "gson" % "2.8.0"