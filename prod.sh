#!/bin/bash

PROD_PATH="prod/web"
UNZP_PATH="prod/webcontroller-1.0-SNAPSHOT"

sbt dist
if [ -d "$PROD_PATH" ]; then
    echo Removing ${PROD_PATH}
    rm -rf ${PROD_PATH}
    mkdir -p ${PROD_PATH}
fi

if [ -d "$UNZP_PATH" ]; then
    echo Removing ${UNZP_PATH}
    rm -rf ${UNZP_PATH}
    mkdir -p ${UNZP_PATH}
fi
mkdir -p ${UNZP_PATH}
unzip target/universal/webcontroller-1.0-SNAPSHOT.zip \
    -d ${UNZP_PATH}
mv ${UNZP_PATH}/* ${PROD_PATH}
rm -rf ${UNZP_PATH}

mv prod/web/webcontroller-1.0-SNAPSHOT/* prod/web/
rm -rf prod/web/webcontroller-1.0-SNAPSHOT
